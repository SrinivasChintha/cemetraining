package com.allstate;

import static org.junit.jupiter.api.Assertions.assertTrue;


import com.allstate.dao.OwnerRepository;

import com.allstate.entities.Owner;
import com.allstate.mongo.MongoJavaConfig;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = MongoJavaConfig.class, loader = AnnotationConfigContextLoader.class)
public class OwnerDaoTest {
    @Autowired
    private OwnerRepository dao;
    
    @Test
    public void testSaveOwner()
    {
        Owner owner = new Owner();
        owner.setName("Deirdre");
        dao.save(owner);
        assertTrue( dao.count()>0);
      
    }
}
