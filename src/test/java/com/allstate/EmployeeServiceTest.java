package com.allstate;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.allstate.dao.Db;
import com.allstate.entities.Employee;
import com.allstate.mongo.MongoJavaConfig;
import com.allstate.service.EmployeeService;
import com.allstate.service.EmployeeServiceImpl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = MongoJavaConfig.class, loader = AnnotationConfigContextLoader.class)
public class EmployeeServiceTest {
    @Autowired
    private EmployeeService service;

    @Test
    public void testSaveEmployee(){
        Employee e1 = new Employee(1, "srini", 22, "chintha", 2000);
        //Employee e2 = new Employee(2, "ann", 22, "d:i.ie", 2000);
        service.save(e1);
        assertTrue(service.getTotal()>0);

    }
}
