package com.allstate.dependencyinjection;

import org.springframework.stereotype.Component;
@Component("Cat")
public class Cat implements Pet{

    @Override
    public void feed() {
        // TODO Auto-generated method stub
        System.out.println("Feed method from Cat");

    }
    
}
