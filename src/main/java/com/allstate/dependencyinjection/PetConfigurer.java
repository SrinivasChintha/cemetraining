package com.allstate.dependencyinjection;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.allstate")
public class PetConfigurer {

    // @Bean
    // public PetOwner petOwner(@Autowired Pet p){
    //     return new PetOwner(p);
    // }
    
    // @Bean
    // public Pet pet(){
    //     return new Cat();
    // }
    
    
}
