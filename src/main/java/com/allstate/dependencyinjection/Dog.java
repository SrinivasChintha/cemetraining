package com.allstate.dependencyinjection;

import org.springframework.stereotype.Component;

@Component("Dog")
public class Dog implements Pet{

    @Override
    public void feed() {
        // TODO Auto-generated method stub
        System.out.println("Feed method from Dog");
    }
    
}
