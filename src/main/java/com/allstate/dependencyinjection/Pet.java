package com.allstate.dependencyinjection;

public interface Pet {
    void feed();
}
