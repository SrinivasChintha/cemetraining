package com.allstate.springlab1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringTest {
    public static void main(String[] args) {
        ApplicationContext context =  new AnnotationConfigApplicationContext(SpringConfiguration.class);
        Car c =context.getBean(Car.class);
        System.out.printf("Engine size %.2f", c.getEngine().getEngineSize());
    }
}
