package com.allstate.springlab1;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.allstate.springlab1")
public class SpringConfiguration {
    
}
