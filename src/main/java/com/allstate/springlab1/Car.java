package com.allstate.springlab1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Car {
    /**
     *
     */
    private static final String DIESEL_ENGINE = "DieselEngine";
    private String make="BMW";
    private String model="3 Series";
    @Autowired
    @Qualifier(DIESEL_ENGINE)
    private Engine engine;

    public Engine getEngine() {
    return engine;
    }
    @Autowired
    @Qualifier("PetrolEngine")
    public void setPetrolEngine(Engine engine) {
    this.engine = engine;
    }
    public String getMake() {
    return make;
    }
    public void setMake(String make) {
    this.make = make;
    }
    public String getModel() {
    return model;
    }
    public void setModel(String model) {
    this.model = model;
    }
    
}
