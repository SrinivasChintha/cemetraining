package com.allstate.springlab1;

public interface Engine {
    double getEngineSize();
    void setEngineSize(double engineSize);
}
