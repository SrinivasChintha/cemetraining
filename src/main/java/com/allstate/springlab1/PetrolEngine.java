package com.allstate.springlab1;

import org.springframework.stereotype.Component;

@Component("PetrolEngine")
public class PetrolEngine implements Engine {
    private double engineSize = 3;

    @Override
    public double getEngineSize() {
    // TODO Auto-generated method stub
    return engineSize;
    }
    @Override
    public void setEngineSize(double engineSize) {
    // TODO Auto-generated method stub
    this.engineSize = engineSize;
    }

}
