package com.allstate.service;

import java.util.List;

import com.allstate.entities.Employee;

public interface EmployeeService {
    long getTotal();

    List<Employee> all();

    void save(Employee employee);

    Employee find(int id);
}
