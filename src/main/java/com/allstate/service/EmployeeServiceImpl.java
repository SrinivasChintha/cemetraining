package com.allstate.service;

import java.util.List;

import com.allstate.dao.Db;
import com.allstate.entities.Employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService {

@Autowired 
private Db dao;


    @Override
    public List<Employee> all() {  
        return dao.findall();
    }

    @Override
    public void save(Employee employee) {
       dao.save(employee);
    }


    @Override
    public long getTotal() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Employee find(int id) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
