package com.allstate.labs;

public class TesetAccount2 {
    public static void main(String[] args) {
        Account[] accounts = new Account[5];
        Account acc1 = new Account(100, "Srinivas");
        Account acc2 = new Account(200, "Anil");
        Account acc3 = new Account(400, "Piyush");
        Account acc4 = new Account(500, "Nishu");
        Account acc5 = new Account(600, "Suman");

        accounts[0]=acc1;
        accounts[1]=acc2;
        accounts[2]=acc3;
        accounts[3]=acc4;
        accounts[4]=acc5;

        Account.setInterestRate(15);

        for(Account account:accounts){
            System.out.printf("Name of the account holder:%s and the the balance is: %.2f \n",account.getName(), account.getBalance());
            System.out.printf("Balance after adding interest:%.2f \n",account.addInterest());
        }
    }
}
