package com.allstate.labs;
public class TestInheritance{
    public static void main(String[] args) {
        Account[] accounts= new Account[3];
        accounts[0]= new Account(2, "Srinivas");
        accounts[1]= new SavingsAccount(4, "Deepthi");
        accounts[2]= new CurrentAccount(6, "Chikku");

        for(Account a:accounts){
            System.out.printf("Interest added for %s and the balance after adding interest amount is %.2f \n",a.getName(),a.addInterest());
        }
    }
}