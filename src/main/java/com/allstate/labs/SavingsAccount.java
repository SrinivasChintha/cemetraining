package com.allstate.labs;

public class SavingsAccount extends Account {

    public SavingsAccount(double balance, String name) {
        super(balance, name);
    }
    
    @Override
    public double addInterest(){
        return getBalance()*1.4;
    }
}
