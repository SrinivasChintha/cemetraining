package com.allstate.labs;

public class CurrentAccount extends Account{

    public CurrentAccount(double balance, String name) {
        super(balance, name);
    }
    
    @Override
    public double addInterest(){
        return this.getBalance()*1.1;
    }
}
