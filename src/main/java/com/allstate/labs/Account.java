package com.allstate.labs;

public class Account {
    private double balance;
    private String name;

    private static double interestRate;
    static{
        interestRate = 10;
    }

    public double getBalance() {
        return balance;
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    
    public  double addInterest(){
        return this.balance+ (this.balance/100)*interestRate;
    }

    public Account(){
        this(1000,"vas");
    }
    public Account(double balance, String name) {
        this.balance = balance;
        this.name = name;
    }

    public boolean withdraw(double amount){
        boolean isSuffientBalance = false;
        if(this.balance>amount){
            this.balance = this.balance -amount;
            isSuffientBalance =true;
        }
        return isSuffientBalance;
    }
    public boolean withdraw(){
        return withdraw(20);
    }
}
