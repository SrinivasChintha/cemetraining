package com.allstate.labs;

public class TestAccount {
    public static void main(String[] args) {
        Account account = new Account();
        account.setName("Savings");
        account.setBalance(10000);
        System.out.printf("Name of the account %s with balance rupees %.2f",account.getName(),account.getBalance());
        System.out.println(); 
        System.out.printf("Balance amount after adding interest %.2f", account.addInterest());
        System.out.println("\n=========================================================");

        Account[] arrayOfAccounts = new Account[5];

        double[] amounts= {23,5444,2,345,34};
        String[] names = {"Picard","Ryker","Worf","Troy","Data"};

        for (int i=0; i <arrayOfAccounts.length; i++) {
            arrayOfAccounts[i]=new Account(amounts[i],names[i]);
            System.out.printf("Account holder name %s and the amount is %f \n",names[i],amounts[i]);
            System.out.printf("Balance after adding interest:%.2f \n",arrayOfAccounts[i].addInterest());
        }
    }
}
