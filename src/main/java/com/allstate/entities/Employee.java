package com.allstate.entities;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Employee extends Person {
    private String email;
    private double salary;

    public String getEmail() {
        return email;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Employee(int id, String name, int age, String email, double salary) {
        super(id, name, age);
        this.email = email;
        this.salary = salary;
    }

    public Employee(String email, double salary) {
        this.email = email;
        this.salary = salary;
    }

    @Override
    public void display(){
        System.out.printf("Name of the person is %s and age is %d, email address is:%s and the salary is %.2f",
        this.getName(), this.getAge(),this.email,this.salary);
    }
    
}
