package com.allstate.entities;


public class Person {
    private int id;
    private String name;
    private int age;
    private final static int ageLimit;

    static {
        ageLimit = 100;
    }

    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void display(){
        System.out.printf("Name of the person is %s and age is %d",this.name, this.age);
    }

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }
    public Person(){}

    public static int getAgeLimit(){
        return ageLimit;
    }
}
